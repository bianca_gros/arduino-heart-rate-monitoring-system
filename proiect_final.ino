#define USE_ARDUINO_INTERRUPTS true 
#include <Adafruit_SSD1306.h>
#include <PulseSensorPlayground.h> 
#include "utility/PulseSensor.h"
#define OLED_Address 0x3C 

PulseSensorPlayground pulseSensor;
Adafruit_SSD1306 oled(128, 64); 
 
int a=0;
int lasta=0;
int lastb=0;

int buzzer = 8 ;
const int pulse = 0; 
const int led = 13; 
int prag = 550; // intervalul intre ceea ce consideram ca fiind puls si ce nu
 
void setup() {
  oled.begin(SSD1306_SWITCHCAPVCC, OLED_Address);
  oled.clearDisplay();
  oled.setTextSize(2);

  pinMode (buzzer, OUTPUT);
  Serial.begin(9600);
   
  pulseSensor.analogInput(pulse);
  pulseSensor.blinkOnPulse(led); // functionalitate de clipire a ledului la fiecare bataie
  pulseSensor.setThreshold(prag);
   
  if (pulseSensor.begin()) {
    Serial.println("We created a pulseSensor Object !");
  }
}
 
void loop()
{
  if(a>127){
    oled.clearDisplay();
    a=0;
    lasta=a;
  }

  int myBPM = pulseSensor.getBeatsPerMinute(); 

  oled.setTextColor(WHITE);

  // constructia grafica a segmentelor de dreapta ce vor fi desenate pe OLED
  int value=analogRead(0);
  int b=60-(value/16);
  oled.writeLine(lasta,lastb,a,b,WHITE);
  // actualizarea valorilor pentru a porni urmatoarea linie de unde s-a terminat anterioara
  lastb=b;
  lasta=a;

  // ceea ce se intampla cand se aude o bataie de inima: afisarea pulsului si sunetul emis de buzzer
  if (pulseSensor.sawStartOfBeat()) { 
    Serial.println("♥ A HeartBeat Happened ! "); 
    Serial.print("BPM: ");
    Serial.println(myBPM); 
    digitalWrite (buzzer, HIGH); 
  }
  digitalWrite (buzzer, LOW); // pauzele semnalului sonor intre 2 batai de inima
  delay(20);
  oled.writeFillRect(0,50,128,16,BLACK); // colorarea fundalului ecranului OLED cu negru
  oled.setCursor(0,50);
  oled.print("BPM:");
  oled.print(myBPM);
   
  oled.display();
  a++;
}
