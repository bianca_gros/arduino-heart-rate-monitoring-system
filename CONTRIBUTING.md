–	Pulse Rate (BPM) Monitor using Arduino & Pulse Sensor
https://how2electronics.com/pulse-rate-bpm-monitor-arduino-pulse-sensor/

–	ECG Display using Pulse Sensor with OLED & Arduino https://how2electronics.com/pulse-sensor-with-oled-arduino/

–	Guide for I2C OLED Display with Arduino
https://randomnerdtutorials.com/guide-for-oled-display-with-arduino/


–	COLOR OLED SSD1331 DISPLAY WITH ARDUINO UNO
https://www.electronics-lab.com/project/color-oled-ssd1331-display-arduino-uno/
